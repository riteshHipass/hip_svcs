/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * HipClientLocationIntegrationProvider generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`HipClientLocationIntegrationProvider`")
public class HipClientLocationIntegrationProvider implements Serializable {

    private Integer id;
    private int clientLocationIntegrationId;
    private String firstName;
    private String lastName;
    private Date dob;
    private String gender;
    private String npi;
    private String ssn;
    private String ehrSystemPhysicianId;
    private String phoneHome;
    private String phoneCell;
    private String phoneWork;
    private String emailId;
    private String specialityKeywords;
    private String address1;
    private String address2;
    private String city;
    private int stateId;
    private String zip;
    private String profileImage;
    private String status = "active";
    private Integer createdBy;
    private Timestamp createdDateTime;
    private Integer updatedBy;
    private Timestamp updatedDateTime;
    private HipState hipState;
    private HipClientLocationIntegration hipClientLocationIntegration;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`clientLocationIntegrationId`", nullable = false, scale = 0, precision = 10)
    public int getClientLocationIntegrationId() {
        return this.clientLocationIntegrationId;
    }

    public void setClientLocationIntegrationId(int clientLocationIntegrationId) {
        this.clientLocationIntegrationId = clientLocationIntegrationId;
    }

    @Column(name = "`firstName`", nullable = false, length = 25)
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "`lastName`", nullable = false, length = 25)
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "`dob`", nullable = true)
    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Column(name = "`gender`", nullable = false, length = 6)
    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "`npi`", nullable = true, length = 50)
    public String getNpi() {
        return this.npi;
    }

    public void setNpi(String npi) {
        this.npi = npi;
    }

    @Column(name = "`ssn`", nullable = true, length = 12)
    public String getSsn() {
        return this.ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    @Column(name = "`ehrSystemPhysicianId`", nullable = false, length = 50)
    public String getEhrSystemPhysicianId() {
        return this.ehrSystemPhysicianId;
    }

    public void setEhrSystemPhysicianId(String ehrSystemPhysicianId) {
        this.ehrSystemPhysicianId = ehrSystemPhysicianId;
    }

    @Column(name = "`phoneHome`", nullable = true, length = 20)
    public String getPhoneHome() {
        return this.phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    @Column(name = "`phoneCell`", nullable = true, length = 20)
    public String getPhoneCell() {
        return this.phoneCell;
    }

    public void setPhoneCell(String phoneCell) {
        this.phoneCell = phoneCell;
    }

    @Column(name = "`phoneWork`", nullable = true, length = 20)
    public String getPhoneWork() {
        return this.phoneWork;
    }

    public void setPhoneWork(String phoneWork) {
        this.phoneWork = phoneWork;
    }

    @Column(name = "`emailId`", nullable = true, length = 25)
    public String getEmailId() {
        return this.emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Column(name = "`specialityKeywords`", nullable = true, length = 250)
    public String getSpecialityKeywords() {
        return this.specialityKeywords;
    }

    public void setSpecialityKeywords(String specialityKeywords) {
        this.specialityKeywords = specialityKeywords;
    }

    @Column(name = "`address1`", nullable = true, length = 100)
    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Column(name = "`address2`", nullable = true, length = 100)
    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Column(name = "`city`", nullable = true, length = 50)
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "`stateId`", nullable = false, scale = 0, precision = 10)
    public int getStateId() {
        return this.stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    @Column(name = "`zip`", nullable = true, length = 10)
    public String getZip() {
        return this.zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Column(name = "`profileImage`", nullable = true, length = 250)
    public String getProfileImage() {
        return this.profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @Column(name = "`status`", nullable = false, length = 8)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "`createdBy`", nullable = true, scale = 0, precision = 10)
    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "`createdDateTime`", nullable = false)
    public Timestamp getCreatedDateTime() {
        return this.createdDateTime;
    }

    public void setCreatedDateTime(Timestamp createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name = "`updatedBy`", nullable = true, scale = 0, precision = 10)
    public Integer getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "`updatedDateTime`", nullable = true)
    public Timestamp getUpdatedDateTime() {
        return this.updatedDateTime;
    }

    public void setUpdatedDateTime(Timestamp updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`stateId`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`fkClientLocationIntegrationProviderStateId`"))
    @Fetch(FetchMode.JOIN)
    public HipState getHipState() {
        return this.hipState;
    }

    public void setHipState(HipState hipState) {
        if(hipState != null) {
            this.stateId = hipState.getId();
        }

        this.hipState = hipState;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`clientLocationIntegrationId`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`fkClientLocationIntegrationProvider`"))
    @Fetch(FetchMode.JOIN)
    public HipClientLocationIntegration getHipClientLocationIntegration() {
        return this.hipClientLocationIntegration;
    }

    public void setHipClientLocationIntegration(HipClientLocationIntegration hipClientLocationIntegration) {
        if(hipClientLocationIntegration != null) {
            this.clientLocationIntegrationId = hipClientLocationIntegration.getId();
        }

        this.hipClientLocationIntegration = hipClientLocationIntegration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HipClientLocationIntegrationProvider)) return false;
        final HipClientLocationIntegrationProvider hipClientLocationIntegrationProvider = (HipClientLocationIntegrationProvider) o;
        return Objects.equals(getId(), hipClientLocationIntegrationProvider.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}