/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * HipPatientBalanceStg generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`HipPatientBalanceStg`")
public class HipPatientBalanceStg implements Serializable {

    private Integer id;
    private int clientLocationIntegrationId;
    private String patientId;
    private String guarantorId;
    private String guarantorFirstName;
    private String guarantorLastName;
    private String patientBalance;
    private Integer createdBy;
    private Timestamp createdDateTime;
    private String responseFromEhr;
    private HipClientLocationIntegration hipClientLocationIntegration;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`clientLocationIntegrationId`", nullable = false, scale = 0, precision = 10)
    public int getClientLocationIntegrationId() {
        return this.clientLocationIntegrationId;
    }

    public void setClientLocationIntegrationId(int clientLocationIntegrationId) {
        this.clientLocationIntegrationId = clientLocationIntegrationId;
    }

    @Column(name = "`patientId`", nullable = false, length = 50)
    public String getPatientId() {
        return this.patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @Column(name = "`guarantorId`", nullable = false, length = 50)
    public String getGuarantorId() {
        return this.guarantorId;
    }

    public void setGuarantorId(String guarantorId) {
        this.guarantorId = guarantorId;
    }

    @Column(name = "`guarantorFirstName`", nullable = false, length = 50)
    public String getGuarantorFirstName() {
        return this.guarantorFirstName;
    }

    public void setGuarantorFirstName(String guarantorFirstName) {
        this.guarantorFirstName = guarantorFirstName;
    }

    @Column(name = "`guarantorLastName`", nullable = false, length = 50)
    public String getGuarantorLastName() {
        return this.guarantorLastName;
    }

    public void setGuarantorLastName(String guarantorLastName) {
        this.guarantorLastName = guarantorLastName;
    }

    @Column(name = "`patientBalance`", nullable = false, length = 50)
    public String getPatientBalance() {
        return this.patientBalance;
    }

    public void setPatientBalance(String patientBalance) {
        this.patientBalance = patientBalance;
    }

    @Column(name = "`createdBy`", nullable = true, scale = 0, precision = 10)
    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "`createdDateTime`", nullable = false)
    public Timestamp getCreatedDateTime() {
        return this.createdDateTime;
    }

    public void setCreatedDateTime(Timestamp createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name = "`responseFromEhr`", nullable = false, length = 65535)
    public String getResponseFromEhr() {
        return this.responseFromEhr;
    }

    public void setResponseFromEhr(String responseFromEhr) {
        this.responseFromEhr = responseFromEhr;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`clientLocationIntegrationId`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`HipPatientBalanceStg_ibfk_1`"))
    @Fetch(FetchMode.JOIN)
    public HipClientLocationIntegration getHipClientLocationIntegration() {
        return this.hipClientLocationIntegration;
    }

    public void setHipClientLocationIntegration(HipClientLocationIntegration hipClientLocationIntegration) {
        if(hipClientLocationIntegration != null) {
            this.clientLocationIntegrationId = hipClientLocationIntegration.getId();
        }

        this.hipClientLocationIntegration = hipClientLocationIntegration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HipPatientBalanceStg)) return false;
        final HipPatientBalanceStg hipPatientBalanceStg = (HipPatientBalanceStg) o;
        return Objects.equals(getId(), hipPatientBalanceStg.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}