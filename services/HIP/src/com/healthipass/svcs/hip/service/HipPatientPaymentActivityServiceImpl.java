/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.healthipass.svcs.hip.HipPatientPaymentActivity;


/**
 * ServiceImpl object for domain model class HipPatientPaymentActivity.
 *
 * @see HipPatientPaymentActivity
 */
@Service("HIP.HipPatientPaymentActivityService")
@Validated
public class HipPatientPaymentActivityServiceImpl implements HipPatientPaymentActivityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HipPatientPaymentActivityServiceImpl.class);


    @Autowired
    @Qualifier("HIP.HipPatientPaymentActivityDao")
    private WMGenericDao<HipPatientPaymentActivity, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<HipPatientPaymentActivity, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipPatientPaymentActivity create(HipPatientPaymentActivity hipPatientPaymentActivity) {
        LOGGER.debug("Creating a new HipPatientPaymentActivity with information: {}", hipPatientPaymentActivity);

        HipPatientPaymentActivity hipPatientPaymentActivityCreated = this.wmGenericDao.create(hipPatientPaymentActivity);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(hipPatientPaymentActivityCreated);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipPatientPaymentActivity getById(Integer hippatientpaymentactivityId) {
        LOGGER.debug("Finding HipPatientPaymentActivity by id: {}", hippatientpaymentactivityId);
        return this.wmGenericDao.findById(hippatientpaymentactivityId);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipPatientPaymentActivity findById(Integer hippatientpaymentactivityId) {
        LOGGER.debug("Finding HipPatientPaymentActivity by id: {}", hippatientpaymentactivityId);
        try {
            return this.wmGenericDao.findById(hippatientpaymentactivityId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No HipPatientPaymentActivity found with id: {}", hippatientpaymentactivityId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public List<HipPatientPaymentActivity> findByMultipleIds(List<Integer> hippatientpaymentactivityIds, boolean orderedReturn) {
        LOGGER.debug("Finding HipPatientPaymentActivities by ids: {}", hippatientpaymentactivityIds);

        return this.wmGenericDao.findByMultipleIds(hippatientpaymentactivityIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "HIPTransactionManager")
    @Override
    public HipPatientPaymentActivity update(HipPatientPaymentActivity hipPatientPaymentActivity) {
        LOGGER.debug("Updating HipPatientPaymentActivity with information: {}", hipPatientPaymentActivity);

        this.wmGenericDao.update(hipPatientPaymentActivity);
        this.wmGenericDao.refresh(hipPatientPaymentActivity);

        return hipPatientPaymentActivity;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipPatientPaymentActivity delete(Integer hippatientpaymentactivityId) {
        LOGGER.debug("Deleting HipPatientPaymentActivity with id: {}", hippatientpaymentactivityId);
        HipPatientPaymentActivity deleted = this.wmGenericDao.findById(hippatientpaymentactivityId);
        if (deleted == null) {
            LOGGER.debug("No HipPatientPaymentActivity found with id: {}", hippatientpaymentactivityId);
            throw new EntityNotFoundException(String.valueOf(hippatientpaymentactivityId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public void delete(HipPatientPaymentActivity hipPatientPaymentActivity) {
        LOGGER.debug("Deleting HipPatientPaymentActivity with {}", hipPatientPaymentActivity);
        this.wmGenericDao.delete(hipPatientPaymentActivity);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipPatientPaymentActivity> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all HipPatientPaymentActivities");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipPatientPaymentActivity> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all HipPatientPaymentActivities");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service HIP for table HipPatientPaymentActivity to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service HIP for table HipPatientPaymentActivity to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}