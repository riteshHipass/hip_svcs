/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * HipClientLocationIntegrationConfig generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`HipClientLocationIntegrationConfig`")
public class HipClientLocationIntegrationConfig implements Serializable {

    private Integer id;
    private int integrationServiceId;
    private Integer clientLocationIntegrationId;
    private String key;
    private String value;
    private String description;
    private String status = "hold";
    private Integer createdBy;
    private Timestamp createdDateTime;
    private Integer updatedBy;
    private Timestamp updatedDateTime;
    private HipClientLocationIntegration hipClientLocationIntegration;
    private HipIntegrationServiceList hipIntegrationServiceList;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`integrationServiceId`", nullable = false, scale = 0, precision = 10)
    public int getIntegrationServiceId() {
        return this.integrationServiceId;
    }

    public void setIntegrationServiceId(int integrationServiceId) {
        this.integrationServiceId = integrationServiceId;
    }

    @Column(name = "`clientLocationIntegrationId`", nullable = true, scale = 0, precision = 10)
    public Integer getClientLocationIntegrationId() {
        return this.clientLocationIntegrationId;
    }

    public void setClientLocationIntegrationId(Integer clientLocationIntegrationId) {
        this.clientLocationIntegrationId = clientLocationIntegrationId;
    }

    @Column(name = "`key`", nullable = false, length = 30)
    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Column(name = "`value`", nullable = false, length = 50)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "`description`", nullable = false, length = 250)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "`status`", nullable = true, length = 8)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "`createdBy`", nullable = true, scale = 0, precision = 10)
    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "`createdDateTime`", nullable = false)
    public Timestamp getCreatedDateTime() {
        return this.createdDateTime;
    }

    public void setCreatedDateTime(Timestamp createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name = "`updatedBy`", nullable = true, scale = 0, precision = 10)
    public Integer getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "`updatedDateTime`", nullable = true)
    public Timestamp getUpdatedDateTime() {
        return this.updatedDateTime;
    }

    public void setUpdatedDateTime(Timestamp updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`clientLocationIntegrationId`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`fkHipClientLocationIntegrationConfigClientLocationIntegrationId`"))
    @Fetch(FetchMode.JOIN)
    public HipClientLocationIntegration getHipClientLocationIntegration() {
        return this.hipClientLocationIntegration;
    }

    public void setHipClientLocationIntegration(HipClientLocationIntegration hipClientLocationIntegration) {
        if(hipClientLocationIntegration != null) {
            this.clientLocationIntegrationId = hipClientLocationIntegration.getId();
        }

        this.hipClientLocationIntegration = hipClientLocationIntegration;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`integrationServiceId`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`fkHipClientLocationIntegrationConfigIntegrationServiceId`"))
    @Fetch(FetchMode.JOIN)
    public HipIntegrationServiceList getHipIntegrationServiceList() {
        return this.hipIntegrationServiceList;
    }

    public void setHipIntegrationServiceList(HipIntegrationServiceList hipIntegrationServiceList) {
        if(hipIntegrationServiceList != null) {
            this.integrationServiceId = hipIntegrationServiceList.getId();
        }

        this.hipIntegrationServiceList = hipIntegrationServiceList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HipClientLocationIntegrationConfig)) return false;
        final HipClientLocationIntegrationConfig hipClientLocationIntegrationConfig = (HipClientLocationIntegrationConfig) o;
        return Objects.equals(getId(), hipClientLocationIntegrationConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}